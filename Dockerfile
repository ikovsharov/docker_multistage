FROM debian:9 as build
RUN apt update && apt install -y wget gcc make libpcre3 libpcre3-dev zlib1g zlib1g-dev

RUN wget https://github.com/openresty/luajit2/archive/refs/tags/v2.1-20210510.tar.gz && tar xvfz v2.1-20210510.tar.gz && cd luajit2-2.1-20210510 && make && make install
RUN wget https://github.com/vision5/ngx_devel_kit/archive/refs/tags/v0.3.1.tar.gz && tar xvfz v0.3.1.tar.gz && mv ngx_devel_kit-0.3.1 /ngx_devel_kit
RUN wget https://github.com/openresty/lua-nginx-module/archive/refs/tags/v0.10.19.tar.gz && tar xvfz v0.10.19.tar.gz && mv lua-nginx-module-0.10.19 /lua-nginx-module
RUN wget https://github.com/openresty/lua-resty-core/archive/refs/tags/v0.1.21.tar.gz && tar xvfz v0.1.21.tar.gz && cd lua-resty-core-0.1.21 && make && make install
RUN wget https://github.com/openresty/lua-resty-lrucache/archive/refs/tags/v0.10.tar.gz && tar xvfz v0.10.tar.gz && cd lua-resty-lrucache-0.10 && make && make install
ENV LUAJIT_LIB=/usr/local/lib
ENV LUAJIT_INC=/usr/local/include/luajit-2.1
RUN wget https://nginx.org/download/nginx-1.19.3.tar.gz && tar xvfz nginx-1.19.3.tar.gz && cd nginx-1.19.3 && ./configure --with-ld-opt="-Wl,-rpath,/usr/local/lib" --add-module=/ngx_devel_kit --add-module=/lua-nginx-module && make && make install

FROM debian:9
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx/sbin/nginx .
COPY --from=build /usr/local/lib/ /usr/local/lib/
RUN mkdir ../logs ../conf && touch ../logs/error.log && chmod +x nginx
CMD ["./nginx", "-g", "daemon off;"]
